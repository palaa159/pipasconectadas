'use strict';

/**
 * @ngdoc overview
 * @name pipasApp
 * @description
 * # pipasApp
 *
 * Main module of the application.
 */
angular
  .module('pipasApp', [
    'ui.router',
    'app.controller.home',
    'app.controller.allflights',
    'app.controller.competition',
    'app.controller.about',
    'app.controller.upload',
    'app.controller.profile',
    'app.services',
    'app.configs',
    'app.router',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularMoment',
    'flow'
  ])
  .run(function($rootScope, Facebook) {
    console.log('init');
    // Auto-login if there is accessToken memorized
    // Login
    $rootScope.login = function() {
      console.log('Logging in');
      Facebook.login(function(res) {
        console.log(res);
      });
    };
  });