'use strict';

angular.module('app.router', [])
  .config(function($stateProvider,
    $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      .state('allflights', {
        url: '/allflights',
        templateUrl: 'views/allflights.html',
        controller: 'AllFlightsCtrl'
      })
      .state('competition', {
        url: '/competition',
        templateUrl: 'views/competition.html',
        controller: 'CompetitionCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .state('upload', {
        url: '/upload',
        templateUrl: 'views/upload.html',
        controller: 'UploadCtrl'
      })
      .state('profile', {
        url: '/profile/:facebookid',
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl'
      });

    // Default URL
    $urlRouterProvider.otherwise('/');
  });