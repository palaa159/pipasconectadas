'use strict';

/**
 * @ngdoc function
 * @name pipasApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pipasApp
 */
angular.module('app.controller.about', [])
  .controller('AboutCtrl', function($scope) {
    console.log('about init');

    // Team
    $scope.team = [{
      img: 'http://placekitten.com/g/500/300',
      name: 'Claudia Bernett',
      role: 'Directora de Criação',
      email: 'claudia@littlecloudcollective.com'
    },{
      img: 'http://placekitten.com/g/500/300',
      name: 'Sander Santiago',
      role: 'Produção Executivo',
      email: 'sanderjsantiago@gmail.com'
    },{
      img: 'http://placekitten.com/g/500/300',
      name: 'Jennifer Matsumoto',
      role: 'Diretora de Marca',
      email: 'jennifer@littlecloudcollective.com'
    },{
      img: 'http://placekitten.com/g/500/300',
      name: 'Apon Palanuwech',
      role: 'Directora de Tecnologia',
      email: 'reach.apon@gmail.com'
    },{
      img: 'http://placekitten.com/g/500/300',
      name: 'Nick Lyell',
      role: 'Directora de Visualização',
      email: 'njlyell@gmail.com'
    }];
  });