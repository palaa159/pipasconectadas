'use strict';

/**
 * @ngdoc function
 * @name pipasApp.controller:UploadCtrl
 * @description
 * # UploadCtrl
 * Controller of the pipasApp
 */
angular.module('app.controller.upload', ['flow'])
  .controller('UploadCtrl', function($scope, $state) {
    console.log('upload init');

    $scope.added = function(flowObj) {
      console.log(flowObj);
      if (flowObj.file.type === 'text/plain') {
        // Extract and Upload
        var reader = new FileReader();
        reader.onload = function() {
          console.log(reader.result);
        };
        reader.readAsText(flowObj.file);
      } else {
        // Alert Error
        alert('Sorry! wrong file type. TXT pls.');
        $state.go($state.current, {}, {
          reload: true
        });
      }
    };
  });