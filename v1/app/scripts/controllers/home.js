'use strict';

/**
 * @ngdoc function
 * @name pipasApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the pipasApp
 */
angular.module('app.controller.home', [])
  .controller('HomeCtrl', function($scope) {
    console.log('init');
  });